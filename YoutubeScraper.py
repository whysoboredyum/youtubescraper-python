from async_timeout import timeout
from requests_html import HTMLSession
from bs4 import BeautifulSoup as bs
import urllib.request
import json
import re
import csv

#Usamos la libreria urllib para obtener la respuesta de nuestra busqueda de Youtube
html = urllib.request.urlopen("https://www.youtube.com/results?search_query=Leslie+Lamport")
video_ids = re.findall(r"watch\?v=(\S{11})", html.read().decode()) #guardamos en una lista todos los 11 digitos para el URL de video de youtube
video_url = "https://www.youtube.com/watch?v=" + video_ids[0] #utilizamos solo el 1er resultado

# iniciamos sesion 
session = HTMLSession()

def get_video_info(url):
    # descargar el HTML
    response = session.get(url)
    # ejecutar Javascript
    response.html.render(sleep=1,timeout=60)
    # Creamos un objeto beautifulsoup para parsear el HTML
    soup = bs(response.html.html, "html.parser")
    # Inicializar diccionario de resultados
    result = {}

    result["Titulo"] = soup.find("meta", itemprop="name")['content']
    result["Nro Vistas"] = soup.find("meta", itemprop="interactionCount")['content']
    result["Descripcion"] = soup.find("meta", itemprop="description")['content']
    result["Fecha de Publicacion"] = soup.find("meta", itemprop="datePublished")['content']
    result["Duracion del Video"] = soup.find("span", {"class": "ytp-time-duration"}).text
    result['Autor'] = soup.find("span", itemprop="author").next.next['content']

    #Visualizar likes
    data = re.search(r"var ytInitialData = ({.*?});", soup.prettify()).group(1)
    data_json = json.loads(data)
    videoPrimaryInfoRenderer = data_json['contents']['twoColumnWatchNextResults']['results']['results']['contents'][0]['videoPrimaryInfoRenderer']
    videoSecondaryInfoRenderer = data_json['contents']['twoColumnWatchNextResults']['results']['results']['contents'][1]['videoSecondaryInfoRenderer']
    # Numero de likes
    likes_label = videoPrimaryInfoRenderer['videoActions']['menuRenderer']['topLevelButtons'][0]['toggleButtonRenderer']['defaultText']['accessibility']['accessibilityData']['label'] # "No likes" or "###,### likes"
    likes_str = likes_label.split(' ')[0].replace(',','')
    result["Likes"] = '0' if likes_str == 'No' else likes_str
    return result


# ejecutamos la funcion de obtener datos
datos = get_video_info(video_url)
# Imprimimos 
print(f"Titulo: {datos['Titulo']}")
print(f"Nro Vistas: {datos['Nro Vistas']}")
print(f"Fecha de Publicacion: {datos['Fecha de Publicacion']}")
print(f"Duracion del Video: {datos['Duracion del Video']}")
print(f"Likes: {datos['Likes']}")
print(f"Descripcion: {datos['Descripcion']}")
print(f"Nombre del Canal autor: {datos['Autor']}")

#Generamos un archivo CSV que guarda los datos de nuestro diccionario
csv_file = "DATOS.csv"
with open(csv_file, 'w',encoding="utf-8") as f:
        for key in datos.keys():
            f.write("%s, %s\n" % (key, datos[key]))
